/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <time.h>
#include <gtk/gtk.h>

#include <glib.h>
#include <gdk/gdkkeysyms.h>
#include <gst/gst.h>		
#include <gst/interfaces/xoverlay.h>
#include <gdk/gdkx.h>

#include "../common/globales.h"

#ifndef proyeccion_video
#define proyeccion_video 


//gboolean bus_callback(GstBus * bus, GstMessage *msg, gpointer data);
static void realize_cb (GtkWidget * widget, CustomData *data);//function to create video window
static gboolean on_key_press(GtkWidget *widget,GdkEventKey *event , CustomData *data); //evento de teclado
static gboolean expose_event(GtkWidget *widget, GdkEventExpose *event,CustomData *data);
static void error_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void eos_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static GstBusSyncReply
bus_sync_handler (GstBus * bus, GstMessage * message, CustomData *data);
int show_proyeccion_vid (GtkWidget *widget, int mode);

#endif
