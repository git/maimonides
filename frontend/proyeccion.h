/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <time.h>
#include <gtk/gtk.h>
#include <cairo.h>
#include <gdk/gdkkeysyms.h>
#include <gst/gst.h>		
#include <gst/interfaces/xoverlay.h>

#ifndef proyeccion
#define proyeccion 
int mode;
//gboolean bus_callback(GstBus * bus, GstMessage *msg, gpointer data);
static gboolean time_handler(GtkWidget *widget);  //evento de tiempo, disparado cada segundo
static gboolean retry_playing(CustomData *data); //evento de tiempo, disparado cada segundo
static gboolean on_key_press(GtkWidget *widget,GdkEventKey *event , CustomData *data); //evento de teclado
static gboolean on_key_press_nosound(GtkWidget *widget,GdkEventKey *event,CustomData *data); //evento de teclado sin sonido
static gboolean on_expose_event(GtkWidget *widget,GdkEventKey *event,gpointer data);
static void error_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void eos_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data);

int show_proyeccion (GtkWidget *widget, int mode);

#endif
