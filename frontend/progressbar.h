/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef carga
#define carga 
struct strProgressBarData
{
  int mode;
  gpointer *win;
  struct pista * audios_index;
  struct imagen *image_index;
  struct pista * videos_index;
}DATA_progressbar;

static gboolean destroy_win(gpointer data);
static gboolean load_done(gpointer data);
gboolean update_progress(gpointer data);
gpointer call_fscripts_FILES (gpointer data);
gpointer call_fscripts_FUSB (gpointer data);
gpointer call_fscripts_TUSB (gpointer data);

int load_biblioteca(gpointer mode);




#endif
