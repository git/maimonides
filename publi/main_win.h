/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef main_window
#define main_window

static void start_show(GtkWidget *widget);
static void show_about(GtkWidget *widget, gpointer data);
static void mode_changed(GtkWidget *combobox, int* mode);
static GdkPixbuf *create_pixbuf(const gchar * filename);
void clear_textview ();
void actualize_textview ();
void refresh_text (GtkWidget *widget);

int main_win (int init_mode);

#endif
