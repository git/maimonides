/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <cairo.h>

#include "../common/globales.h"

#ifndef _H_carga_biblioteca
#define _H_carga_biblioteca
int carga_usb() ;
int guarda_usb() ;
int return_app_path (gchar **path);
int carga_recursos_biblioteca (int MODE,struct imagen** image_index,struct pista** audio_index,struct pista** video_index,struct glob_settings** settings);
int cargar_XML_biblioteca (int MODE, struct imagen** image_index,struct pista** audio_index,struct pista** video_index,struct glob_settings** settings);

#endif


