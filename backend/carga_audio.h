/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <cairo.h>
#include <sys/types.h>
#include <dirent.h>

#include "../common/globales.h"

#ifndef _h_carga_audios
#define _h_carga_audios

int carga_audios_from_local (struct pista** audios_index);
int carga_audios_from_XML (struct pista** audios_index);
int carga_audio (int MODE,struct pista** audios_index);
int filtro_pistas(strPista** tracks_to_analyze,gchar* ruta);//filtrado de pistas (solo OGG y MP3 por ahora)
int ismp3(char* name);
int isogg(char* name);
#endif


