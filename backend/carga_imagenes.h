/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <cairo.h>

#include "../common/globales.h"

#ifndef _h_carga_imagenes
#define _h_carga_imagenes
int isjpeg(char* name);
int topng_3(char* name);
int topng_4(char* name);
int isjpg(char* name);
int isgif(char* name);
int ispng(char* name);
int set_image_resolution(char* image_name,int width,int height);
int filtro_imagenes(strImagen** images_to_analyze, gchar* ruta);

int carga_imagenes_from_local (struct imagen** image_index);
int carga_imagenes_from_XML (struct imagen** image_index);
int carga_imagenes(int MODE,struct imagen** image_index);

#endif


