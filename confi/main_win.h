/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef main_window
#define main_window

static void start_show(GtkWidget *widget);
static void show_about(GtkWidget *widget, gpointer data);
static void mode_changed(GtkWidget *combobox, int* mode);
static GdkPixbuf *create_pixbuf(const gchar * filename);
static void init_list(GtkWidget *list);
void clear_textview (GtkTextBuffer *buffer);
void actualize_textview (GtkTextBuffer *buffer);
void refresh_text (GtkWidget *widget,GtkTextBuffer *buffer);
gboolean refresh_prestypebox();
void init_main ();
gboolean refresh_tables();
static void  on_enttiempo_changed(GtkWidget *entrytext,gpointer selection);
static void actualize_audlist ();
static void actualize_imalist ();
static void actualize_vidlist ();
int main_win (int init_mode);

#endif
