/*
 * this file is part of Maimonides Suite - advertisment/information player via embedded Raspberry Pi
 *
 * Copyright 2012-2019 by Joaquín Cuéllar <joa.cuellar (at) riseup (dot) net>.
 *
 * Maimonides Suite is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, 
 * either version 3 of the License, or (at your option) any later version.
 *
 * Maimonides Suite is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Maimonides Suite.  
 * If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>

#include "globales.h"
#ifndef pi_parse_xml
#define pi_parse_xml



static void processNode(xmlTextReaderPtr reader, int *section,
			int *subsection,int* parameter,struct imagen** ,struct pista** ,struct pista**,struct glob_settings** );
int populateDoc(char *docname,struct imagen** image_index,struct pista** audios_index,struct pista** videos_index,struct glob_settings** settings);

int recoverDoc(char *docname,struct imagen **image_index,struct pista **audios_index,struct pista **videos_index,struct glob_settings** settings);
#endif
